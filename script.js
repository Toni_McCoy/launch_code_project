//http://bootstrapvalidator.votintsev.ru/settings/?firstName=&lastName=&phone=#field-container-html-tab
// To use feedback icons, ensure that you use Bootstrap v3.1.0 or later
$(document).ready(function() {
  $('#contact_form').bootstrapValidator({
      message: 'This value is not valid',
      feedbackIcons: {
          valid: 'glyphicon glyphicon-ok',
          invalid: 'glyphicon glyphicon-remove',
          validating: 'glyphicon glyphicon-refresh'
      },
      fields: {
          first_name: {
              validators: {
                  stringLength: {
                    min: 2,
                  },
                  notEmpty: {
                    message: 'Please enter your First Name'
              }
            }
          },
          last_name: {
            validators: {
                stringLength: {
                  min: 2,
                },
                notEmpty: {
                  message: 'Please enter your Last Name'
            }
          }
        },
          email: {
                validators: {
                    notEmpty: {
                        message: 'The email is required and cannot be empty'
                    },
                    emailAddress: {
                        message: 'The input is not a valid email address'
                    }
                }
          },
          emplid: {
              validators:{
                stringLength:{
                  max: 7
                },
                notEmpty:{
                  message: 'Please enter your Empl ID'
                },
                regexp:{
                  regexp: /\d{7}/,
                  message: 'Invalid Emplid'
                },
              }
           }
      }
  });
});